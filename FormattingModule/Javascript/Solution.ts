// Naming
// Example 1
class AreaCalcClassSolution {
    // LC2 - Not readable and abbreviations don't make sense
    shp_Len: number;
    shp_Wth: number;

    // LC2 - Inconsistent naming of functions and abbreviations
    calc_Area(): number {
        return this.shp_Len * this.shp_Wth;
    }

    getPerimeter(): number {
        return 2 * this.shp_Wth + 2 * this.shp_Len;
    }

    // LC2 - Naming of function is not clear
    // LC2 - Parameter names are not clear
    set(a, b): void {
        this.shp_Len = a;
        this.shp_Wth = b;
    }

    // LC2 - Naming overrides built in function
    toString(): string {
        return `The shape has a length of ${this.shp_Len} and a width of ${this.shp_Wth}`;
    }
}
    
// Example 2
class CarSolution {
    model: string;
    make: string;
    year: number;
    // LC2 - inconsistent casing of private variable
    // LC2 - putting 'string' in name
    VINString: string;
    // LC2 - inconsistent casing of public variable
    // LC2 - putting 'list' in name
    PartsList: [];
    // LC2 - inconsistent casing of private variable
    Owner: Owner;

    // LC2 - inconsistent naming with similar Parts method
    changeOwner(owner): void {
        this.Owner = owner;
    }
}

class PartsSolution {
    // LC2 - Manufactured is repeated which makes searching more difficult
    manufacturedYear: number;
    manufacturedCountry: string;
    quality: number;
    // LC2 - inconsistent scheme of private variable with car
    _owner: Owner;

    // LC2 - inconsistent naming with similar Car method
    switchOwner(owner): void {
        this._owner = owner;
    }
}
    
// Comments
// LC2 - bad comment, mostly just rambling and personal comments
// A class that represents an electrician. This is mostly just used as an example
// of what electricians do, but I don't have any real in depth knowledge of it so
// the terminology may not be exact.
class ElectricianSoltuion {
    _cert: Certificate;

    // LC2 - not needed
    // An array representing the electrician's tools
    tools: [];

    // LC2 - not needed
    /// <summary>
    /// The public default constructor for the class. It can take a
    /// certificate, but it is not required
    /// </summary>
    constructor(cert?: Certificate) {

    }

    // LC2 - TODO comments are acceptable
    // TODO add a constructor for electrician's with a degree

    // LC2 - forced comments are not always needed
    /// <summary>
    /// Will change a broken lightbulb. You can use tools 
    /// </summary>
    /// LC2 - the wattage comment is not needed, but the second part does provide some clarity
    /// <param name="wattage">The wattage of the lightbulb. Needed so we know what lightbulb to select</param>
    /// <returns>Whether it was successful or not</returns>
    changeLightbulb(wattage: number): void {
        this.turnOffLight();
        // LC2 - this is a clarity comment so should stay
        // It may be overkill to turn off the breaker, but it is a requirement of the company
        this.turnOffBreaker();
        this.unscrewBulb();
        this.screwinBulb(wattage);
        this.turnOnBreaker();
        this.turnOnLight();
        this.checkLight();
    }

    // LC2 - this lets us know of consequences, so should stay
    /// <summary>
    /// This is a complicated process, so it can take some time
    /// </summary>
    /// <returns></returns>
    replaceBreakerBox(): boolean {
        return true;
    }

    // LC2 - this method name could be changed to get rid of comment
    /// <summary>
    /// This will test an outlet in the house to see if it is working and grounded properly
    /// </summary>
    /// LC2 - not needed, it is implied
    /// <param name="outlet">The outlet to be tested</param>
    /// <returns></returns>
    test(outlet: Outlet): boolean {
        return true;
    }

    turnOffLight() { }
    turnOffBreaker() { }
    unscrewBulb() { }
    screwinBulb(wattage: number) { }
    turnOnBreaker() { }
    turnOnLight() { }
    checkLight() { }
}

// Formatting
class GoogleSheetsServiceSolution {
    _credential: ServiceAccountCredential;
    service: SheetsService;
    _applicationName = "Test";
    _spreadsheetId = "testId";
    _aliases: {};
    _transactions: Transaction[];

    constructor()
    {
        this.service = new SheetsService(this._credential, this._applicationName);
    }

    getAllCategories(): string[]
    {
        if (this._transactions == null)
            this.getTransactions();
        var list = this._transactions.map(x => x.category);
        list.sort();
        return list;
    }

    getAllLocations(): string[]
    {
        if (this._transactions == null)
            this.getTransactions();
        var list = this._transactions.map(x => x.location);
        list.sort();
        return list;
    }

    getTransactions(date = null): Transaction[]
    {
        if (this._transactions == null || this._transactions.length != this.getRowCount("Data"))
        {
            this._transactions = [];
            var values = this.getData("Data");
            values.slice(1).forEach(value => {
                this._transactions.push(
                    new Transaction(
                        new Date(value[0].toString()),
                        value[1].toString(), 
                        value[2].toString(), 
                        parseFloat(value[3].toString().replace("$", ""))
                ));
            });
        }
        if (date == null)
            return this._transactions;
        else
            return this._transactions.filter(t => t.date >= date);
    }

    getAliases(): {}
    {
        if (this._aliases == null || Object.keys(this._aliases).length != this.getRowCount("Aliases"))
        {
            this._aliases = {};
            var values = this.getData("Aliases");
            values.forEach(value => 
                this._aliases[value[0].toString()] = value[1].toString()
                );

        }
        return this._aliases;
    }

    getRowCount(sheet: string): number
    {
        return this.getData(sheet).length;
    }

    getData(sheet): Value[]
    {
        var request = this.service.spreadsheets.values.get(this._spreadsheetId, `${sheet}!A1:Z`);
        var response = request.execute();
        return response.values;
    }

    addAlias(alias: Alias): void
    {
        var appendValues = [
            [alias.aliasName, alias.location ]
        ]
        this.sendList(appendValues, "Location Names");
    }

    addTransactions(newvalues: Transaction[]): void
    {
        var appendValues = [];

        newvalues.forEach(value => {
            appendValues.push(
                [`'${value.date}`, value.location, value.category, `$${value.price}`]
            )
        });
        this.sendList(appendValues, "Data");
    }

    sendList(values: any[], sheet: string): void
    {
        var vr = new ValueRange(values);
        var request = this.service.spreadsheets.values.push(
            vr, this._spreadsheetId, `"${sheet}!A${this.getRowCount(sheet)}:B`
        );
        request.execute();
    }
}
