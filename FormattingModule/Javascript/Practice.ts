﻿// Naming
// Example 1
class AreaCalcClass {
    shp_Len: number;
    shp_Wth: number;

    calc_Area(): number {
        return this.shp_Len * this.shp_Wth;
    }

    getPerimeter(): number {
        return 2 * this.shp_Wth + 2 * this.shp_Len;
    }

    set(a, b): void {
        this.shp_Len = a;
        this.shp_Wth = b;
    }

    toString(): string {
        return `The shape has a length of ${this.shp_Len} and a width of ${this.shp_Wth}`;
    }
}

// Example 2
class Car {
    model: string;
    make: string;
    year: string;
    VINString: string;
    PartsList: [];
    Owner: Owner;

    changeOwner(owner): void {
        this.Owner = owner;
    }
}

class Parts {
    manufacturedYear: number;
    manufacturedCountry: string;
    quality: number;
    _owner: Owner;

    switchOwner(owner): void {
        this._owner = owner;
    }
}

// Comments
// A class that represents an electrician. This is mostly just used as an example
// of what electricians do, but I don't have any real in depth knowledge of it so
// the terminology may not be exact.
class Electrician {
    _cert: Certificate;

    // An array representing the electrician's tools
    tools: [];

    /// <summary>
    /// The public default constructor for the class. It can take a
    /// certificate, but it is not required
    /// </summary>
    constructor(cert?: Certificate) {

    }

    // TODO add a constructor for electrician's with a degree

    /// <summary>
    /// Will change a broken lightbulb. You can use tools 
    /// </summary>
    /// <param name="wattage">The wattage of the lightbulb. Needed so we know what lightbulb to select</param>
    /// <returns>Whether it was successful or not</returns>
    changeLightbulb(wattage: number): void {
        this.turnOffLight();
        // It may be overkill to turn off the breaker, but it is a requirement of the company
        this.turnOffBreaker();
        this.unscrewBulb();
        this.screwinBulb(wattage);
        this.turnOnBreaker();
        this.turnOnLight();
        this.checkLight();
    }

    /// <summary>
    /// This is a complicated process, so it can take some time
    /// </summary>
    /// <returns></returns>
    replaceBreakerBox(): boolean {
        return true;
    }

    /// <summary>
    /// This will test an outlet in the house to see if it is working and grounded properly
    /// </summary>
    /// <param name="outlet">The outlet to be tested</param>
    /// <returns></returns>
    test(outlet: Outlet): boolean {
        return true;
    }

    turnOffLight() { }
    turnOffBreaker() { }
    unscrewBulb() { }
    screwinBulb(wattage: number) { }
    turnOnBreaker() { }
    turnOnLight() { }
    checkLight() { }
}

// Formatting
class GoogleSheetsService {
    addAlias(alias: Alias): void {
        var appendValues = [
            [alias.aliasName, alias.location ]
        ]
        this.sendList(appendValues, "Location Names");
    }

    addTransactions(newvalues: Transaction[]): void {
        var appendValues = [];

        newvalues.forEach(value => {
            appendValues.push([`'${value.date}`, value.location, value.category, `$${value.price}`])
        });
        this.sendList(appendValues, "Data");
    }

    _aliases: {};
    _applicationName = "Test";

    _credential: ServiceAccountCredential;

    getAllCategories(): string[] {
        if (this._transactions == null)
            this.getTransactions();
        var list = this._transactions.map(x => x.category);
        list.sort();
        return list;
    }

    getAllLocations(): string[] {
        if (this._transactions == null)
            this.getTransactions();
        var list = this._transactions.map(x => x.location);
        list.sort();
        return list;
    }

    getAliases(): {} {
        if (this._aliases == null || Object.keys(this._aliases).length != this.getRowCount("Aliases")) {
            this._aliases = {};
            var values = this.getData("Aliases");
            values.forEach(value =>
                this._aliases[value[0].toString()] = value[1].toString()
            );
        }
        return this._aliases;
    }

    getData(sheet): Value[]
    {
        var request = this.service.spreadsheets.values.get(this._spreadsheetId, `${sheet}!A1:Z`);
        var response = request.execute();
        return response.values;
    }

    getRowCount(sheet): number
    {
        return this.getData(sheet).length;
    }

    getTransactions(date = null): Transaction[]
    {
        if (this._transactions == null || this._transactions.length != this.getRowCount("Data")) {
            this._transactions = [];
            var values = this.getData("Data");
            values.slice(1).forEach(value => {
                this._transactions.push(new Transaction(new Date(value[0].toString()), value[1].toString(), value[2].toString(), parseFloat(value[3].toString().replace("$", ""))));
            });
        }
        if (date == null)
            return this._transactions;
        else
            return this._transactions.filter(t => t.date >= date);
    }

    constructor()
    {
        this.service = new SheetsService(this._credential, this._applicationName);
    }

    sendList(values: any[], sheet: string): void {
        var vr = new ValueRange(values);
        var request = this.service.spreadsheets.values.push(
            vr, this._spreadsheetId, `"${sheet}!A${this.getRowCount(sheet)}:B`
        );
        request.execute();
    }

    service: SheetsService;
    _spreadsheetId = "testId";
    _transactions: Transaction[];
}
