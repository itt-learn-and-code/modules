﻿using System;
using System.Collections.Generic;

namespace FormattingModule
{
    public class Owner { }
    public class Tools { }
    public enum Certificate { }
    public class Outlet { }
    public class SheetsService
    {
        public Spreadsheet Spreadsheets { get; set; }

        public SheetsService(ServiceAccountCredential cred, string name) { }
    }
    public class Spreadsheet
    {
        public SpreadsheetValues Values { get; set; }
    }
    public class SpreadsheetValues
    {
        public Request Get(string id, string location)
        {
            return new Request();
        }

        public Request Append(ValueRange vr, string id, string location)
        {
            return new Request();
        }
    }
    public class Request
    {
        public Response Execute()
        {
            return new Response();
        }
    }
    public class Response
    {
        public IList<IList<object>> Values { get; set; }
    }
    public class ServiceAccountCredential { }
    public class TransactionViewModel
    {
        public DateTime Date { get; set; }
        public string Location { get; set; }
        public string Category { get; set; }
        public double Price { get; set; }
    }
    public class ValueRange
    {
        public IList<IList<object>> Values { get; set; }
    }
    public class AliasViewModel
    {
        public string AliasName { get; set; }
        public string Location { get; set; }
    }
}
