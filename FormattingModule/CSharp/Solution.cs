﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FormattingModule
{
    #region Naming
    public class AreaCalcClassSolution
    {
        // LC2 - Not readable and abbreviations don't make sense
        public int Shp_Len;
        public int Shp_Wth;

        // LC2 - Inconsistent naming of functions and abbreviations
        public int Calc_Area()
        {
            return Shp_Len * Shp_Wth;
        }

        public int GetPerimeter()
        {
            return 2 * Shp_Wth + 2 * Shp_Len;
        }

        // LC2 - Naming of function is not clear
        // LC2 - Parameter names are not clear
        public void Set(int a, int b)
        {
            Shp_Len = a;
            Shp_Wth = b;
        }

        // LC2 - Naming overrides built in function
        public string ToString()
        {
            return $"The shape has a length of {Shp_Len} and a width of {Shp_Wth}";
        }
    }
    
    public class CarSolution
    {
        public string Model;
        public string Make;
        public int Year;
        // LC2 - inconsistent casing of private variable
        // LC2 - putting 'string' in name
        private string VINString;
        // LC2 - inconsistent casing of public variable
        // LC2 - putting 'list' in name
        public Parts[] partsList;
        // LC2 - inconsistent casing of private variable
        private Owner Owner;

        // LC2 - inconsistent naming with similar Parts method
        public void ChangeOwner(Owner owner)
        {
            Owner = owner;
        }
    }

    public class PartsSolution
    {
        // LC2 - Manufactured is repeated which makes searching more difficult
        public int ManufacturedYear;
        public string ManufacturedCountry;
        public decimal Quality;
        // LC2 - inconsistent scheme of private variable with car
        private Owner _owner;

        // LC2 - inconsistent naming with similar Car method
        public void SwitchOwner(Owner owner)
        {
            _owner = owner;
        }
    }
    #endregion

    #region Comments
    // LC2 - bad comment, mostly just rambling and personal comments
    // A class that represents an electrician. This is mostly just used as an example
    // of what electricians do, but I don't have any real in depth knowledge of it so
    // the terminology may not be exact.
    public class ElectricianSoltuion
    {
        private readonly Certificate _cert;

        // LC2 - not needed
        // An array representing the electrician's tools
        public Tools[] Tools;

        // LC2 - not needed
        /// <summary>
        /// The public default constructor for the class
        /// </summary>
        public ElectricianSoltuion()
        {

        }

        // LC2 - not needed
        // Constructor for when the electrician has a certificate
        public ElectricianSoltuion(Certificate cert)
        {
            _cert = cert;
        }

        // LC2 - TODO comments are acceptable
        // TODO add a constructor for electrician's with a degree

        // LC2 - forced comments are not always needed
        /// <summary>
        /// Will change a broken lightbulb. You can use tools 
        /// </summary>
        /// LC2 - the wattage comment is not needed, but the second part does provide some clarity
        /// <param name="wattage">The wattage of the lightbulb. Needed so we know what lightbulb to select</param>
        /// <returns>Whether it was successful or not</returns>
        public bool ChangeLightbulb(int wattage)
        {
            TurnOffLight();
            // LC2 - this is a clarity comment so should stay
            // It may be overkill to turn off the breaker, but it is a requirement of the company
            TurnOffBreaker();
            UnscrewBulb();
            ScrewinBulb(wattage);
            TurnOnBreaker();
            TurnOnLight();
            return CheckLight();
        }

        // LC2 - this lets us know of consequences, so should stay
        /// <summary>
        /// This is a complicated process, so it can take some time
        /// </summary>
        /// <returns></returns>
        public bool ReplaceBreakerBox()
        {
            return true;
        }

        // LC2 - this method name could be changed to get rid of comment
        /// <summary>
        /// This will test an outlet in the house to see if it is working and grounded properly
        /// </summary>
        /// LC2 - not needed, it is implied
        /// <param name="outlet">The outlet to be tested</param>
        /// <returns></returns>
        public bool Test(Outlet outlet)
        {
            return true;
        }

        #region BuildHelpers
        public void TurnOffLight() { }
        public void TurnOffBreaker() { }
        public void UnscrewBulb() { }
        public void ScrewinBulb(int wattage) { }
        public void TurnOnBreaker() { }
        public void TurnOnLight() { }
        public bool CheckLight() { return true; }
        #endregion
    }
    #endregion

    #region Formatting
    public class GoogleSheetsServiceSolution
    {
        private ServiceAccountCredential _credential = new ServiceAccountCredential();
        public SheetsService Service;
        private string _applicationName = "Test";
        private string _spreadsheetId = "testId";
        private Dictionary<string, string> _aliases;
        private List<TransactionViewModel> _transactions;

        public GoogleSheetsServiceSolution()
        {
            Service = new SheetsService(_credential, _applicationName);
        }

        public IEnumerable<string> GetAllCategories()
        {
            if (_transactions == null)
                GetTransactions();
            var list = _transactions.Select(x => x.Category).Distinct().ToList();
            list.Sort();
            return list;
        }

        public IEnumerable<string> GetAllLocations()
        {
            if (_transactions == null)
                GetTransactions();
            var list = _transactions.Select(x => x.Location).Distinct().ToList();
            list.Sort();
            return list;
        }

        public IEnumerable<TransactionViewModel> GetTransactions(DateTime? date = null)
        {
            if (_transactions == null || _transactions.Count != GetRowCount("Data"))
            {
                _transactions = new List<TransactionViewModel>();
                IList<IList<object>> values = GetData("Data");
                foreach (var val in values.Skip(1))
                {
                    _transactions.Add(new TransactionViewModel()
                    {
                        Date = DateTime.ParseExact(
                            val[0].ToString().TrimStart('\''),
                            "yyyyMMdd",
                            System.Globalization.CultureInfo.InvariantCulture),
                        Location = val[1].ToString(),
                        Category = val[2].ToString(),
                        Price = double.Parse(val[3].ToString().Replace("$", ""))
                    });
                }

            }
            if (date == null)
                return _transactions
                    .OrderBy(t => t.Date)
                    .ThenBy(t => t.Location)
                    .ThenBy(t => t.Price).ToList();
            else
                return _transactions
                    .Where(t => t.Date >= date)
                    .OrderBy(t => t.Date)
                    .ThenBy(t => t.Location)
                    .ThenBy(t => t.Price).ToList();
        }

        public Dictionary<string, string> GetAliases()
        {
            if (_aliases == null || _aliases.Count != GetRowCount("Aliases"))
            {
                _aliases = new Dictionary<string, string>();
                IList<IList<object>> values = GetData("Aliases");
                foreach (var val in values)
                {
                    _aliases[val[0].ToString()] = val[1].ToString();
                }

            }
            return _aliases;
        }

        public int GetRowCount(string sheet)
        {
            return GetData(sheet).Count;
        }

        private IList<IList<object>> GetData(string sheet)
        {
            var request = Service.Spreadsheets.Values.Get(_spreadsheetId, $"{sheet}!A1:Z");
            var response = request.Execute();
            return response.Values;
        }

        public void AddAlias(AliasViewModel alias)
        {
            var appendValues = new List<IList<object>>
        {
            new List<object> { alias.AliasName, alias.Location }
        };
            SendList(appendValues, "Location Names");
        }

        public void AddTransactions(IEnumerable<TransactionViewModel> newvalues)
        {
            var appendValues = new List<IList<object>>();
            foreach (var data in newvalues)
            {
                appendValues.Add(new List<object> {
                    $"'{data.Date:yyyyMMdd}", data.Location, data.Category, $"${data.Price}"
                });
            }
            SendList(appendValues, "Data");
        }

        private void SendList(IList<IList<object>> values, string sheet)
        {
            ValueRange vr = new ValueRange()
            {
                Values = values
            };
            var request = Service.Spreadsheets.Values.Append(
                vr, _spreadsheetId, $"{sheet}!A{GetRowCount(sheet)}:B");
            request.Execute();
        }
    }
    #endregion
}
