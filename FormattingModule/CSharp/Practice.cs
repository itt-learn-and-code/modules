﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FormattingModule
{
    #region Naming
    // Example 1
    public class AreaCalcClass
    {
        public int Shp_Len;
        public int Shp_Wth;

        public int Calc_Area()
        {
        return Shp_Len * Shp_Wth;
        }

        public int GetPerimeter()
        {
            return 2 * Shp_Wth + 2 * Shp_Len;
        }

        public void Set(int a, int b)
        {
            Shp_Len = a;
            Shp_Wth = b;
        }

        public string ToString()
        {
            return $"The shape has a length of {Shp_Len} and a width of {Shp_Wth}";
        }
    }

    // Example 2
    public class Car
    {
        public string Model;
        public string Make;
        public int Year;
        private string VINString;
        public Parts[] partsList;
        private Owner Owner;

        public void ChangeOwner(Owner owner)
        {
            Owner = owner;
        }
    }

    public class Parts
    {
        public int ManufacturedYear;
        public string ManufacturedCountry;
        public decimal Quality;
        private Owner _owner;

        public void SwitchOwner(Owner owner)
        {
            _owner = owner;
        }
    }
    #endregion

    #region Comments
    // A class that represents an electrician. This is mostly just used as an example
    // of what electricians do, but I don't have any real in depth knowledge of it so
    // the terminology may not be exact.
    public class Electrician
    {
        private readonly Certificate _cert;

        // An array representing the electrician's tools
        public Tools[] Tools;

        /// <summary>
        /// The public default constructor for the class
        /// </summary>
        public Electrician()
        {

        }

        // Constructor for when the electrician has a certificate
        public Electrician(Certificate cert)
        {
            _cert = cert;
        }

        // TODO add a constructor for electrician's with a degree

        /// <summary>
        /// Will change a broken lightbulb. You can use tools 
        /// </summary>
        /// <param name="wattage">The wattage of the lightbulb. Needed so we know what lightbulb to select</param>
        /// <returns>Whether it was successful or not</returns>
        public bool ChangeLightbulb(int wattage)
        {
            TurnOffLight();
            // It may be overkill to turn off the breaker, but it is a requirement of the company
            TurnOffBreaker();
            UnscrewBulb();
            ScrewinBulb(wattage);
            TurnOnBreaker();
            TurnOnLight();
            return CheckLight();
        }

        /// <summary>
        /// This is a complicated process, so it can take some time
        /// </summary>
        /// <returns></returns>
        public bool ReplaceBreakerBox()
        {
            return true;
        }

        /// <summary>
        /// This will test an outlet in the house to see if it is working and grounded properly
        /// </summary>
        /// <param name="outlet">The outlet to be tested</param>
        /// <returns></returns>
        public bool Test(Outlet outlet)
        {
            return true;
        }

        #region BuildHelpers
        public void TurnOffLight() { }
        public void TurnOffBreaker() { }
        public void UnscrewBulb() { }
        public void ScrewinBulb(int wattage) { }
        public void TurnOnBreaker() { }
        public void TurnOnLight() { }
        public bool CheckLight() { return true; }
        #endregion
    }
    #endregion

    #region Formatting
    public class GoogleSheetsService
    {
        public void AddAlias(AliasViewModel alias)
        {
            var appendValues = new List<IList<object>>
            {
                new List<object> { alias.AliasName, alias.Location }
            };
            SendList(appendValues, "Location Names");
        }

        public void AddTransactions(IEnumerable<TransactionViewModel> newvalues)
        {
            var appendValues = new List<IList<object>>();
            foreach (var data in newvalues)
            {
                appendValues.Add(new List<object> { $"'{data.Date:yyyyMMdd}", data.Location, data.Category, $"${data.Price}" });
            }
            SendList(appendValues, "Data");
        }

        private Dictionary<string, string> _aliases;
        private string _applicationName = "Test";

        private ServiceAccountCredential _credential = new ServiceAccountCredential();

        public IEnumerable<string> GetAllCategories()
        {
            if (_transactions == null)
                GetTransactions();
            var list = _transactions.Select(x => x.Category).Distinct().ToList();
            list.Sort();
            return list;
        }

        public IEnumerable<string> GetAllLocations()
        {
            if (_transactions == null)
                GetTransactions();
            var list = _transactions.Select(x => x.Location).Distinct().ToList();
            list.Sort();
            return list;
        }

        public Dictionary<string, string> GetAliases()
        {
            if (_aliases == null || _aliases.Count != GetRowCount("Aliases"))
            {
                _aliases = new Dictionary<string, string>();
                IList<IList<object>> values = GetData("Aliases");
                foreach (var val in values)
                {
                    _aliases[val[0].ToString()] = val[1].ToString();
                }

            }
            return _aliases;
        }

        private IList<IList<object>> GetData(string sheet)
        {
            var request = Service.Spreadsheets.Values.Get(_spreadsheetId, $"{sheet}!A1:Z");
            var response = request.Execute();
            return response.Values;
        }

        public int GetRowCount(string sheet)
        {
            return GetData(sheet).Count;
        }

        public IEnumerable<TransactionViewModel> GetTransactions(DateTime? date = null)
        {
            if (_transactions == null || _transactions.Count != GetRowCount("Data"))
            {
                _transactions = new List<TransactionViewModel>();
                IList<IList<object>> values = GetData("Data");
                foreach (var val in values.Skip(1))
                {
                    _transactions.Add(new TransactionViewModel()
                    {
                        Date = DateTime.ParseExact(val[0].ToString().TrimStart('\''), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture),
                        Location = val[1].ToString(),
                        Category = val[2].ToString(),
                        Price = double.Parse(val[3].ToString().Replace("$", ""))
                    });
                }

            }
            if (date == null)
                return _transactions.OrderBy(t => t.Date).ThenBy(t => t.Location).ThenBy(t => t.Price).ToList();
            else
                return _transactions.Where(t => t.Date >= date).OrderBy(t => t.Date).ThenBy(t => t.Location).ThenBy(t => t.Price).ToList();
        }

        public GoogleSheetsService()
        {
            Service = new SheetsService(_credential, _applicationName);
        }

        private void SendList(IList<IList<object>> values, string sheet)
        {
            ValueRange vr = new ValueRange()
            {
                Values = values
            };
            var request = Service.Spreadsheets.Values.Append(vr, _spreadsheetId, $"{sheet}!A{GetRowCount(sheet)}:B");
            request.Execute();
        }

        public SheetsService Service;
        private string _spreadsheetId = "testId";
        private List<TransactionViewModel> _transactions;
    }
    #endregion
}
