﻿class ControllerBase { }
class RequestBase {
    name: string;
    email: string;
    password: string;
    validate(requiredFields: string[]): boolean { return true; }
    type: PaymentType;
}
class UserResponse extends Response {
    constructor(data: any) {
        super();
    }
}
class User {
    name: string;
    email: string;
    password: string;
    createdAt: Date;

    constructor(name?: string, email?: string, password?: string, date?: Date) {

    }

    save(): void { }
    getAllUsers(): User[] { return []; }
}
abstract class Security {
    static encrypt(password: string): string { return password; }
}
class StoreRequest {
    type: PaymentType;
}
class Payment {
    payWithCreditCard(): void { }
    payWithPaypal(): void { }
    payWithWireTransfer(): void { }
}
enum PaymentType {
    Credit, Paypal, WireTransfer
}
class Person {
    squeezeDuck(duck: any): boolean { return true; }
    throwDuckInTub(duck: any): boolean { return true; }
}
class Entity {

}
abstract class MailService {
    static sendEmail(email: string, message: string): void { }
}
class UsersResponse extends Response {
    constructor(data: any) {
        super();
    }
}