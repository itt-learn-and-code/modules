﻿// Single Responsibility Principle
class StoreUserRequest {
    name: string;
    email: string;
    password: string;

    public constructor(name: string, email: string, password: string) {
        this.name = name;
        this.email = email;
        this.password = password;
        if (!this.validate())
            throw new Error("Invalid request!");
    }

    public validate(): boolean {
        return true;
    }

    public requiredFields(): string[] {
        return [
            "name",
            "email",
            "password"
        ];
    }
}

class UserRepository {
    public create(userData: StoreUserRequest): User {
        var user = new User(
            userData.name,
            userData.email,
            Security.encrypt(userData.password)
        );
        user.save();
        return user;
    }
}

class ControllerSolution extends ControllerBase
{
    public store(request: StoreUserRequest, userRepository: UserRepository): Response {
        var user = userRepository.create(request);
        return new UserResponse(user);
    }
}

// Open / Closed Principle
interface IPayable {
    pay(): void;
}

class CreditCardPayment implements IPayable
{
    public pay(): void { }
}

class PaypalPayment implements IPayable
{
    public pay(): void { }
}

class WirePayment implements IPayable
{
    public pay(): void { }
}


class PaymentFactory {
    public initializePayment(type: PaymentType): IPayable {
        if (type == PaymentType.Credit)
            return new CreditCardPayment();
        else if (type == PaymentType.Paypal)
            return new PaypalPayment();
        else if (type == PaymentType.WireTransfer)
            return new WirePayment();
        else
            throw new Error("Unsupported payment method!");
    }
}

class StoreSolution {
    public pay(request: RequestBase) {
        var paymentFactory = new PaymentFactory();
        var payment = paymentFactory.initializePayment(request.type);
        payment.pay();
    }
}

// Liskov Substitution Principle
interface IQuackable {
    quack(): string;
}

interface IFlyable {
    fly(): string;
}

interface ISwimmable {
    swim(): string;
}

class RubberDuckSolution implements IQuackable, ISwimmable
{
    public quack(): string {
        var person = new Person();

        if (person.squeezeDuck(this))
            return "The duck is quacking!";
        else
            throw new Error("A rubber duck can't quack on its own.");
    }

    public swim(): string {
        var person = new Person();

        if (person.throwDuckInTub(this))
            return "The duck is swimming!";
        else
            throw new Error("A rubber duck can't swim on its own.");
    }
}

// Interface Segregation Principle
interface INotifiable {
    getNotifyEmail(): string;
}

class NotificationsSolution {
    public send(subscriber: INotifiable, message: string): void {
        MailService.sendEmail(subscriber.getNotifyEmail(), message);
    }
}

// Dependency Inversion Principle
interface IUserRepository {
    getAfterDate(date: Date): User[];
    create(userData: StoreUserRequest): User;
}

class DependencyUserRepository implements IUserRepository
{
    private readonly _allUsers: User[];

    public getAfterDate(date: Date): User[] {
        return this._allUsers.filter(x => x.createdAt >= date);
    }

    public create(userData: StoreUserRequest): User {
        var user = new User (
            userData.name,
            userData.email,
            Security.encrypt(userData.password)
        );
        user.save();
        return user;
    }
}

class DependencyControllerSolution extends ControllerBase
{
    public Index(userRepository: IUserRepository): Response {
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        var users = userRepository.getAfterDate(yesterday);
        return new UserResponse(users);
    }
}
