﻿// Single Responsibility Principle
class Controller extends ControllerBase {
    store(request: RequestBase, user: User): Response {
        let valid = request.validate(["name", "email", "password"]);
        if (!valid)
            throw new Error("Invalid request!");
        user.name = request.name;
        user.email = request.email;
        user.password = Security.encrypt(request.password);
        user.save();
        return new UserResponse(user);
    }
}

// Open / Closed Principle
class Store {
    pay(request: StoreRequest): void {
        var payment = new Payment();

        if (request.type == PaymentType.Credit)
            payment.payWithCreditCard();
        else if (request.type == PaymentType.Paypal)
            payment.payWithPaypal();
        else
            payment.payWithWireTransfer();
    }
}

// Liskov Substitution Principle
interface IDuck {
    quack(): string;
    fly(): string;
    swim(): string;
}

class RubberDuck implements IDuck
{
    public quack(): string{
        var person = new Person();

        if (person.squeezeDuck(this))
            return "The duck is quacking!";
        else
            throw new Error("A rubber duck can't quack on its own.");
    }

    public fly(): string {
        throw new Error("A rubber duck can't fly.");
    }

    public swim(): string {
        var person = new Person();

        if (person.throwDuckInTub(this))
            return "The duck is swimming!";
        else
            throw new Error("A rubber duck can't swim on its own.");
    }
}

class Subscriber extends Entity
{
    public subscribe(): void {

    }

    public unsubscribe(): void {

    }

    public getNotifyEmail(): string {
        return "email";
    }
}

class Notifications {
    public send(subscriber: Subscriber, message: string): void {
        MailService.sendEmail(subscriber.getNotifyEmail(), message);
    }
}

// Dependency Inversion Principle
class DependencyController {
    public index(user: User): Response {
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        var users = user.getAllUsers().filter(x => x.createdAt >= yesterday);
        return new UsersResponse(users);
    }
}
