using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SOLIDModule
{
    #region Single Responsibility Principle
    public class Controller : ControllerBase
    {
        public IActionResult Store(Request request, User user)
        {
            bool valid = request.Validate(nameof(user.Name), nameof(user.Email), nameof(user.Password));
            if (!valid)
                throw new InvalidRequestException();
            user.Name = request.Name;
            user.Email = request.Email;
            user.Password = Security.Encrypt(request.Password);
            user.Save();
            return Ok(user);
        }
    }
    #endregion

    #region Open/Closed Principle
    public class Store
    {
        public void Pay(Request request)
        {
            var payment = new Payment();

            if (request.Type == PaymentType.Credit)
                payment.PayWithCreditCard();
            else if (request.Type == PaymentType.Paypal)
                payment.PayWithPaypal();
            else
                payment.PayWithWireTransfer();
        }
    }
    #endregion

    public class CompanyManagementClient
    {
        private ILogger<CompanyManagementClient> Logger { get; }
        private HttpClient Client { get; }

        private async Task<ResultData<CarrierProfileResponseContract>> SendRequest(Guid brokerAccountId,
            Guid carrierAccountId, string authorizationToken, List<Guid> carrierTenderGroupIds, Guid? correlationId = null,
            CancellationToken cancellationToken = default)
        {
            correlationId = correlationId ?? Guid.NewGuid();
            var result = new ResultData<CarrierProfileResponseContract> { Success = false };

            try
            {
                // Create a new request message so we can set the headers per request
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"company/{brokerAccountId}/carrier/{carrierAccountId}"))
                {
                    // Set the headers
                    requestMessage.Headers.Add(HeaderKeys.CorrelationId, correlationId.ToString());
                    requestMessage.Headers.Add(HeaderNames.Authorization, authorizationToken);
                    requestMessage.Content = new StringContent(JsonSerializer.Serialize(carrierTenderGroupIds), Encoding.UTF8, "application/json");

                    // Send the request
                    var httpResult = await Client.SendAsync(requestMessage, cancellationToken);
                    var json = await httpResult.Content.ReadAsStringAsync();

                    try
                    {
                        IList<ResultStatus> responseResultStatusSet = null;

                        if (httpResult.StatusCode != HttpStatusCode.OK)
                        {
                            try
                            {
                                var response = JsonSerializer.Deserialize<ResultData<CarrierProfileResponseContract>>(json);
                                responseResultStatusSet = response.StatusSet;
                            }
                            catch (Exception ex)
                            {
                                Logger.LogException(ex, $"An error occurred while attempting to deserialize the response from {nameof(CarrierProfileResponseContract)} in the {nameof(CompanyManagementClient)}. Response: {json}");
                            }
                        }

                        switch (httpResult.StatusCode)
                        {
                            case HttpStatusCode.Unauthorized:
                                result.StatusCode = GenericResponseCode.Unauthorized;
                                if (responseResultStatusSet != null)
                                {
                                    result.AddStatus(responseResultStatusSet);
                                }
                                break;
                            case HttpStatusCode.NotFound:
                                result.StatusCode = GenericResponseCode.NotFound;
                                if (responseResultStatusSet != null)
                                {
                                    result.AddStatus(responseResultStatusSet);
                                }
                                result.Success = true;
                                break;
                            case HttpStatusCode.InternalServerError:
                                result.StatusCode = GenericResponseCode.BadGateWay;
                                result.StatusSet.Add(CommonResponseCode.ExternalServiceError.AsResultStatus(string.Empty));
                                if (responseResultStatusSet != null)
                                {
                                    result.AddStatus(responseResultStatusSet);
                                }
                                break;
                            case HttpStatusCode.BadRequest:
                                result.StatusCode = GenericResponseCode.InternalError;
                                result.StatusSet.Add(CommonResponseCode.ExternalServiceBadRequest.AsResultStatus());
                                if (responseResultStatusSet != null)
                                {
                                    result.AddStatus(responseResultStatusSet);
                                }
                                Logger.LogError("When calling {0}, received bad response {1}", requestMessage.RequestUri.AbsoluteUri, json);
                                break;
                            case HttpStatusCode.OK:
                                result.Data = JsonSerializer.Deserialize<CarrierProfileResponseContract>(json);
                                result.Success = true;
                                result.StatusCode = GenericResponseCode.Get;
                                break;
                            default:
                                result.StatusCode = GenericResponseCode.BadGateWay;
                                result.StatusSet.Add(ResponseCode.CompanyManagementNoResponse.AsResultStatus());
                                break;
                        }

                        return result;
                    }
                    catch (JsonException ex)
                    {
                        Logger.LogException(ex, $"An error occured while attempting to deserialize the response in the {nameof(CompanyManagementClient)}.");

                        result.AddStatus(CommonResponseCode.FailedToDeserialize.AsResultStatus());
                        result.StatusCode = GenericResponseCode.InternalError;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, $"An error occured while attempting to call base address in the {nameof(CompanyManagementClient)}.");
                result.AddStatus(CommonResponseCode.InternalError.AsResultStatus());
                result.StatusCode = GenericResponseCode.InternalError;
            }

            return result;
        }
    }

    #region Liskov Substitution Principle
    public interface IDuck
    {
        string Quack();
        string Fly();
        string Swim();
    }

    public class RubberDuck : IDuck
    {
        public string Quack()
        {
            var person = new Person();

            if (person.SqueezeDuck(this))
                return "The duck is quacking!";
            else
                throw new Exception("A rubber duck can't quack on its own.");
        }

        public string Fly()
        {
            throw new Exception("A rubber duck can't fly.");
        }

        public string Swim()
        {
            var person = new Person();

            if (person.ThrowDuckInTub(this))
                return "The duck is swimming!";
            else
                throw new Exception("A rubber duck can't swim on its own.");
        }
    }
    #endregion

    #region Interface Segregation Principle
    class Subscriber : Entity
    {
        public void Subscribe()
        {

        }

        public void Unsubscribe()
        {

        }

        public string GetNotifyEmail()
        {
            return "email";
        }
    }

    class Notifications
    {
        public void Send(Subscriber subscriber, string message)
        {
            MailService.SendEmail(subscriber.GetNotifyEmail(), message);
        }
    }
    #endregion

    #region Dependency Inversion Principle
    public class DependencyController : ControllerBase
    {
        public IActionResult Index(User user)
        {
            var users = user.GetAllUsers().Where(x => x.CreatedAt >= DateTime.Today.AddDays(-1));
            return Ok(users);
        }
    }
    #endregion
}