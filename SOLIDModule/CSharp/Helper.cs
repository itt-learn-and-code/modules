﻿using System;
using System.Collections.Generic;

namespace SOLIDModule
{
    public class Request
    {
        public bool Validate(params string[] requiredFields) { return true; }
        public string Name;
        public string Email;
        public string Password;
        public PaymentType Type;
    }

    public enum PaymentType
    {
        Credit, Paypal, WireTransfer
    }

    public class User : Entity
    {
        public string Name;
        public string Email;
        public string Password;
        public void Save() { }
        public List<User> GetAllUsers() { return new List<User>(); }
    }

    public class Security
    {
        public static string Encrypt(string password) { return password; }
    }

    public interface IActionResult { }
    public class Result : IActionResult { }
    public class InvalidRequestException : Exception { }

    public class ControllerBase
    {
        public IActionResult Ok(object obj)
        {
            return new Result();
        }
    }

    public class Payment
    {
        public void PayWithCreditCard() { }
        public void PayWithPaypal() { }
        public void PayWithWireTransfer() { }
    }

    public class Person
    {
        public bool SqueezeDuck(RubberDuck duck) { return true; }
        public bool ThrowDuckInTub(RubberDuck duck) { return true; }

        public bool SqueezeDuck(RubberDuckSolution duck) { return true; }
        public bool ThrowDuckInTub(RubberDuckSolution duck) { return true; }
    }

    public class Entity 
    {
        public DateTime CreatedAt;
    }

    public static class MailService 
    {
        public static void SendEmail(string email, string message) { }
    }

    public class ResultData<T>
    { 
        public bool Success { get; set; }
        public IList<ResultStatus> StatusSet { get; set; }
        public GenericResponseCode StatusCode { get; set; }
        public void AddStatus(IEnumerable<ResultStatus> statusSet)
        {
        }
        public void AddStatus(ResultStatus status)
        { 
        }
        public T Data { get; set; }
    }

    public class CarrierProfileResponseContract
    { 
    }

    public static class HeaderKeys
    {
        public const string CorrelationId = "X-Correlation-ID";
        public const string SenderUserId = "X-SenderUserId";
        public const string ClientId = "X-ClientId";
    }

    public static class HeaderNames
    {
        public const string Authorization = "Authorization";
    }

    public class ResultStatus
    {
    }

    public interface ILogger<T>
    {
        public void LogException(Exception ex, string message);
        public void LogError(string message, string uri, string json);
    }

    public enum GenericResponseCode
    {
        Created = 0,
        Updated = 1,
        Deleted = 2,
        Get = 3,
        Forbidden = 4,
        ValidationFailed = 5,
        InternalError = 6,
        NotFound = 7,
        BadGateWay = 8,
        PartialSuccess = 9,
        Unauthorized = 10,
        NoChange = 11
    }

    public enum CommonResponseCode
    {
        Required = 100,
        OutOfRange = 101,
        OutOfRangeTextLength = 102,
        OutOfRangeList = 103,
        SearchReqConvertFailed = 104,
        InvalidData = 105,
        OK = 200,
        Created = 201,
        Accepted = 202,
        Missing = 204,
        Found = 302,
        NoChange = 304,
        InvalidRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        Timeout = 408,
        ExternalServiceUnauthorized = 409,
        ExternalRecordNotFound = 410,
        InternalError = 500,
        NotImplemented = 501,
        FailedToDeserialize = 502,
        ExternalServiceBadRequest = 503,
        ExternalServiceError = 504,
        ExternalServiceNoResponse = 505
    }

    public enum ResponseCode
    {
        CompanyManagementNoResponse
    }

    public static class ResultExtensions
    {
        public static ResultStatus AsResultStatus(this Enum item, params object[] args)
        {
            return new ResultStatus();
        }
    }
}
