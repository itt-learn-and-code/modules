﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace SOLIDModule
{
    #region Single Responsibility Principle
    public class StoreUserRequest
    {
        public string Name;
        public string Email;
        public string Password;

        public StoreUserRequest(string name, string email, string password)
        {
            Name = name;
            Email = email;
            Password = password;
            if (!Validate())
                throw new InvalidRequestException();
        }

        public bool Validate()
        {
            return true;
        }

        public string[] RequiredFields()
        {
            return new string[]
            {
                nameof(Name),
                nameof(Email),
                nameof(Password)
            };
        }
    }

    class UserRepository
    { 
        public User Create(StoreUserRequest userData)
        {
            var user = new User
            {
                Name = userData.Name,
                Email = userData.Email,
                Password = Security.Encrypt(userData.Password)
            };
            user.Save();
            return user;
        }
    }

    class ControllerSolution : ControllerBase
    {
        public IActionResult Store(StoreUserRequest request, UserRepository userRepository)
        {
            var user = userRepository.Create(request);
            return Ok(user);
        }
    }
    #endregion

    #region Open/Closed Principle
    public interface IPayable
    {
        public void Pay();
    }

    public class CreditCardPayment : IPayable
    {
        public void Pay() { }
    }

    public class PaypalPayment : IPayable
    {
        public void Pay() { }
    }

    public class WirePayment : IPayable
    {
        public void Pay() { }
    }


    public class PaymentFactory
    {
        public IPayable InitializePayment(PaymentType type)
        {
            if (type == PaymentType.Credit)
                return new CreditCardPayment();
            else if (type == PaymentType.Paypal)
                return new PaypalPayment();
            else if (type == PaymentType.WireTransfer)
                return new WirePayment();
            else
                throw new Exception("Unsupported payment method!");
        }
    }

    public class StoreSolution
    {
        public void Pay(Request request)
        {
            var paymentFactory = new PaymentFactory();
            var payment = paymentFactory.InitializePayment(request.Type);
            payment.Pay();
        }
    }
    #endregion

    public class CompanyManagementClientSolution
    {
        private ILogger<CompanyManagementClient> Logger { get; }
        private HttpClient Client { get; }

        private HttpRequestMessage CreateRequest(Guid brokerAccountId,
            Guid carrierAccountId, string authorizationToken, List<Guid> carrierTenderGroupIds, Guid? correlationId = null)
        {
            try
            {
                correlationId = correlationId ?? Guid.NewGuid();

                var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"company/{brokerAccountId}/carrier/{carrierAccountId}");
                requestMessage.Headers.Add(HeaderKeys.CorrelationId, correlationId.ToString());
                requestMessage.Headers.Add(HeaderNames.Authorization, authorizationToken);
                requestMessage.Content = new StringContent(JsonSerializer.Serialize(carrierTenderGroupIds), Encoding.UTF8, "application/json");
                return requestMessage;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, $"An error occured while attempting to call base address in the {nameof(CompanyManagementClient)}.");
                return null;
            }

        }

        public class ProcessParameters
        {
            public GenericResponseCode Status { get; set; }
            public bool Success { get; set; }
            public bool AddResponseResultStatusSet { get; set; }
            public ResultStatus AdditionalStatus { get; set; }

            public ProcessParameters(GenericResponseCode status, bool success, bool add, ResultStatus resultStatus = null)
            {
                Status = status;
                AddResponseResultStatusSet = add;
                Success = success;
                AdditionalStatus = resultStatus;
            }

            public void SetResultInformation(ResultData<CarrierProfileResponseContract> result, IList<ResultStatus> statusSet)
            {
                result.StatusCode = Status;
                if (AdditionalStatus != null)
                    result.StatusSet.Add(AdditionalStatus);
                if (AddResponseResultStatusSet && statusSet != null)
                    result.AddStatus(statusSet);
                result.Success = Success;
            }
        }

        public Dictionary<HttpStatusCode, ProcessParameters> ProcessorInformation = new Dictionary<HttpStatusCode, ProcessParameters>
        {
            { HttpStatusCode.OK, new ProcessParameters(GenericResponseCode.Get, true, false) },
            { HttpStatusCode.Unauthorized, new ProcessParameters(GenericResponseCode.Unauthorized, false, true) },
            { HttpStatusCode.NotFound, new ProcessParameters(GenericResponseCode.NotFound, true, true) },
            { HttpStatusCode.InternalServerError, new ProcessParameters(GenericResponseCode.BadGateWay, false, true, CommonResponseCode.ExternalServiceError.AsResultStatus(string.Empty)) },
            { HttpStatusCode.BadRequest, new ProcessParameters(GenericResponseCode.InternalError, false, true, CommonResponseCode.ExternalServiceBadRequest.AsResultStatus()) }
        };

        private async Task<ResultData<CarrierProfileResponseContract>> ProcessResponse(HttpResponseMessage httpResult, string uri)
        {
            var result = new ResultData<CarrierProfileResponseContract> { Success = false };

            if (httpResult == null)
            {
                result.AddStatus(CommonResponseCode.InternalError.AsResultStatus());
                result.StatusCode = GenericResponseCode.InternalError;
                return result;
            }

            var json = await httpResult.Content.ReadAsStringAsync();

            try
            {
                IList<ResultStatus> responseResultStatusSet = null;

                if (httpResult.StatusCode != HttpStatusCode.OK)
                {
                    var response = JsonSerializer.Deserialize<ResultData<CarrierProfileResponseContract>>(json);
                    responseResultStatusSet = response.StatusSet;
                }
                else
                {
                    result.Data = JsonSerializer.Deserialize<CarrierProfileResponseContract>(json);
                }

                if (ProcessorInformation.ContainsKey(httpResult.StatusCode))
                {
                    ProcessorInformation[httpResult.StatusCode].SetResultInformation(result, responseResultStatusSet);
                }
                else
                {
                    result.StatusCode = GenericResponseCode.BadGateWay;
                    result.StatusSet.Add(ResponseCode.CompanyManagementNoResponse.AsResultStatus());
                }

                return result;
            }
            catch (JsonException ex)
            {
                Logger.LogException(ex, $"An error occured while attempting to deserialize the response in the {nameof(CompanyManagementClient)}.");

                result.AddStatus(CommonResponseCode.FailedToDeserialize.AsResultStatus());
                result.StatusCode = GenericResponseCode.InternalError;
            }
            return result;
        }

        private async Task<ResultData<CarrierProfileResponseContract>> ProcessRequest(Guid brokerAccountId,
            Guid carrierAccountId, string authorizationToken, List<Guid> carrierTenderGroupIds, Guid? correlationId = null,
            CancellationToken cancellationToken = default)
        {
            // Create a new request message so we can set the headers per request
            using (var requestMessage = CreateRequest(brokerAccountId, carrierAccountId, authorizationToken, carrierTenderGroupIds, correlationId))
            {
                // Send the request
                var httpResult = await Client.SendAsync(requestMessage, cancellationToken);
                var result = await ProcessResponse(httpResult, requestMessage.RequestUri.AbsoluteUri);
                return result;
            }
        }
    }

    #region Liskov Substitution Principle
    public interface IQuackable
    {
        public string Quack();
    }

    public interface IFlyable
    {
        public string Fly();
    }

    public interface ISwimmable
    {
        public string Swim();
    }

    public class RubberDuckSolution : IQuackable, ISwimmable
    {
        public string Quack()
        {
            var person = new Person();

            if (person.SqueezeDuck(this))
                return "The duck is quacking!";
            else
                throw new Exception("A rubber duck can't quack on its own.");
        }

        public string Swim()
        {
            var person = new Person();

            if (person.ThrowDuckInTub(this))
                return "The duck is swimming!";
            else
                throw new Exception("A rubber duck can't swim on its own.");
        }
    }
    #endregion

    #region Interface Segregation Principle
    interface INotifiable
    {
        public string GetNotifyEmail();
    }

    class NotificationsSolution
    {
        public void Send(INotifiable subscriber, string message)
        {
            MailService.SendEmail(subscriber.GetNotifyEmail(), message);
        }
    }
    #endregion

    #region Dependency Inversion Principle
    public interface IUserRepository
    {
        public IEnumerable<User> GetAfterDate(DateTime date);
        public User Create(StoreUserRequest userData);
    }

    class DependencyUserRepository : IUserRepository
    {
        private readonly List<User> _allUsers;

        public IEnumerable<User> GetAfterDate(DateTime date)
        {
            return _allUsers.Where(x => x.CreatedAt >= DateTime.Now.AddDays(-1));
        }

        public User Create(StoreUserRequest userData)
        {
            var user = new User
            {
                Name = userData.Name,
                Email = userData.Email,
                Password = Security.Encrypt(userData.Password)
            };
            user.Save();
            return user;
        }
    }

    public class DependencyControllerSolution : ControllerBase
    {
        public IActionResult Index(IUserRepository userRepository)
        {
            var users = userRepository.GetAfterDate(DateTime.Today.AddDays(-1));
            return Ok(users);
        }
    }
    #endregion
}
