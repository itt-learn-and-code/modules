﻿class Example {
    calcCalories<T>(e: T) : number {
        return 100;
    }

    isBreadToasted(sandwhich: Sandwhich): boolean {
        return true;
    }

    isBreadBurnt(sandwhich: Sandwhich): boolean {
        return false;
    }

    addStatecodeMappingToNewLoction(location: LocationInformation): LocationInformation {
        return location;
    }

    async addTimezoneRegionToNewLocationAsync(location: LocationInformation, adapter: LocationAdapter): Promise<LocationInformation> {
        return await location;
    }
}

class Sandwhich {
    meat: Meat;
    bread: Bread;
    lettuce: boolean;
    tomato: boolean;
    pickles: boolean;
    bacon: boolean;
    isGlutenFree: boolean;
    calories: number;
    isToasted: boolean;

    constructor(meat: Meat, bread: Bread, lettuce: boolean, tomato: boolean, pickles: boolean, bacon: boolean)
    {
        this.meat = meat;
        this.bread = bread;
        this.lettuce = lettuce;
        this.tomato = tomato;
        this.pickles = pickles;
        this.bacon = bacon;
    }

    toToaster() : void { }
    toast() : void { }
    serveWithNoBread() : void { }
    serveWithWhiteBread() : void { }
    serveWithWheatBread() : void { }
    serve() : void { }
}

enum Meat {
    None, Ham, Turkey
}

enum Bread {
    None, White, Wheat
}

enum Toppings {
    Lettuce, Tomato, Pickles, Bacon
}

class LocationInformation {
    address: string;
    address2: string;
    city: string;
    stateCode: string;
    zipCode: string;
    countryCode: string;
    timeZone: string;
    region: string;
    area: string;
    areaNumber: string;

    asDataModel(): LocationDataModel {
        return new LocationDataModel();
    }
}

class LocationDataModel extends LocationInformation
{
}

class LocationAdapter {
    checkLocation: LocationDataModel = null;

    async getLocationAsync(location: LocationDataModel): Promise<LocationDataModel> {
        return await location;
    }

    async getLocationByAddressAsync(location: LocationDataModel): Promise<LocationDataModel> {
        return await location;
    }

    async insertAsync(location: LocationInformation): Promise<void> {
        return;
    }
}

class Repository {
    getDetailsById(placeId: string, sessionToken: string): LocationInformation {
        return new LocationInformation();
    }
}
class LocationDataAdapterFactory {
    create(): LocationAdapter {
        return new LocationAdapter();
    }
}

class RepositoryFileReader {
    async read(fileName: string): Promise<string> {
        return "content";
    }
}

class GetObjectRequest {
    bucketName: string;
    key: string;
}

class AmazonResponse implements IDisposable {
    responseStream: ResponseStream
    headers: ResponseHeaders

    dispose(): void { }
}

interface IDisposable {
    dispose(): void;
}


class ResponseStream {
    copyTo(stream: MemoryStream): void {

    }
}

class MemoryStream {
    position: number;
}

class ResponseHeaders {
    contentType: string;
}

class MemoryStreamWithContentType
{
    public MemoryStreamWithContentType(stream: MemoryStream, content: string) { }
}

class AmazonS3Client {
    static async getObjectAsync(request: GetObjectRequest): Promise<AmazonResponse> {
        return await new AmazonResponse();
    }
}

class Commodity {
    id: number;
    name: string;
}

class Equipment {
    id: number;
    code: string;
    definition: string;
    category: string;
}

class EquipmentOption {
    id: number;
    code: string;
    description: string;
}

class FreightClass {
    id: string;
    value: number;
    weight: string;
    description: string;
}

class TransportationMode {
    id: number;
    code: string;
    description: string;
    partialOrFull: string;
    rank: number;
}

function getDataFromStream(stream: MemoryStream, type: string): string {
    return "data";
}
