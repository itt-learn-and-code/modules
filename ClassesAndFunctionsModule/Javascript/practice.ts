﻿class SandwhichExample extends Example
{
    sandwhichMaker(meat: Meat, bread: Bread, lettuce: boolean, tomato: boolean, pickles: boolean, bacon: boolean): void {
        let sandwhich = new Sandwhich(meat, bread, lettuce, tomato, pickles, bacon);
        sandwhich.isGlutenFree = bread == Bread.None;
        sandwhich.calories = this.calcCalories(meat) + this.calcCalories(bread) + (lettuce ? this.calcCalories(Toppings.Lettuce) : 0) + (tomato ? this.calcCalories(Toppings.Tomato) : 0) + (pickles ? this.calcCalories(Toppings.Pickles) : 0) + (bacon ? this.calcCalories(Toppings.Bacon) : 0);
        if (sandwhich.bread != Bread.None) {
            sandwhich.toToaster();
            while (!sandwhich.isToasted) {
                sandwhich.toast();
                sandwhich.isToasted = this.isBreadToasted(sandwhich);
                if (this.isBreadBurnt(sandwhich))
                    break;
            }
        }
        switch (sandwhich.bread) {
            case Bread.None:
                sandwhich.serveWithNoBread();
                break;
            case Bread.White:
                sandwhich.serveWithWhiteBread();
                break;
            case Bread.Wheat:
                sandwhich.serveWithWheatBread();
                break;
            default:
                sandwhich.serve();
                break;
        }
    }
}

class DetailsExample extends Example
{
    _repo = new Repository();
    _locationDataAdapterFactory = new LocationDataAdapterFactory();

    async GetDetailsById(placeId: string, sessionToken: string): Promise<LocationDataModel> {
        if (!placeId)
            return null;

        if (!sessionToken)
            return null;

        try {
            var location = this._repo.getDetailsById(placeId, sessionToken);

            if (location != null) {
                location = this.addStatecodeMappingToNewLoction(location);

                var dbLocation: LocationDataModel = null;
                var checkLocation: LocationDataModel  = null;

                var adapter = this._locationDataAdapterFactory.create();
                var locationModel = new LocationDataModel();
                locationModel.city = location.city;
                locationModel.stateCode = location.stateCode;
                locationModel.zipCode = location.zipCode;
                locationModel.countryCode = location.countryCode;
                dbLocation = await adapter.getLocationAsync(locationModel);

                if (location.address) {
                    adapter = this._locationDataAdapterFactory.create();
                    locationModel.address = location.address;
                    locationModel.address2 = location.address2;
                    locationModel.city = location.city;
                    locationModel.stateCode = location.stateCode;
                    locationModel.zipCode = location.zipCode;
                    locationModel.countryCode = location.countryCode;
                    checkLocation = await adapter.getLocationByAddressAsync(locationModel);
                }

                if (!location.address) {
                    checkLocation = dbLocation;
                }

                if (dbLocation != null || dbLocation?.city) {
                    location.timeZone = dbLocation.timeZone;
                    location.region = dbLocation.region;
                    location.area = dbLocation.area;
                    location.areaNumber = dbLocation.areaNumber;
                }

                if (dbLocation == null) {
                    adapter = this._locationDataAdapterFactory.create();
                    location = await this.addTimezoneRegionToNewLocationAsync(location, adapter);
                }

                if (checkLocation == null || !checkLocation?.city) {
                    adapter = this._locationDataAdapterFactory.create();
                    await adapter.insertAsync(location);
                }
                else {
                    location = checkLocation;
                }

                return location.asDataModel();
            }
        }
        catch (error)
        {
            throw new Error(error);
        }

        return null;
    }

}

class SampleRepository {
    _bucketName: string;

    constructor(bucketName: string) {
        this._bucketName = bucketName;
    }

    async readData<T>(fileName: string): Promise<T[]> {
        try {
            if (!fileName) {
                throw new Error(fileName);
            }

            var json = await this.readDataFromBucket(fileName);
            var items = JSON.parse(json);
            if (!items)
                throw new Error("Invalid data");

            return items;
        }
        catch (error)
        {
            throw new Error(error);
        }
    }

    async readDataFromBucket(fileName: string): Promise<string> {
        try {
            if (this._bucketName == null) {
                throw new Error(`Invalid BucketName ${this._bucketName}`);
            }

            var request = new GetObjectRequest();
            request.bucketName = this._bucketName;
            request.key = fileName;

            var response = await AmazonS3Client.getObjectAsync(request);
            var memoryStream = new MemoryStream();
            response.responseStream.copyTo(memoryStream);
            memoryStream.position = 0;
            return getDataFromStream(memoryStream, response.headers.contentType);
        }
        catch (error)
        {
            throw new Error(error);
        }
    }

    commodityById: Map<number, Commodity>;
    commodityByName: Map<string, Commodity>;
    cachedCommodities: Commodity[];

    async reloadCommodityCache(fileName: string): Promise<void> {
        this.cachedCommodities = await this.readData<Commodity>(fileName);
        this.commodityById.clear();
        this.commodityByName.clear();
        this.cachedCommodities.forEach(c => {
            this.commodityById[c.id] = c;
            this.commodityByName[c.name] = c;
        });
    }

    cacheCommodity(item: Commodity): void {
        this.commodityById[item.id] = item;
        this.commodityByName[item.name] = item;
    }

    getCommodityById(id: number): Commodity {
        return this.commodityById[id];
    }

    getCommodityByName(name: string): Commodity {
        return this.commodityByName[name];
    }

    getCommoditiesFromCache = () => this.cachedCommodities;

    getCommodityFromCacheById(id: number): Commodity {
        if (id == 0) {
            throw new Error("Invalid data.");
        }

        var result = this.getCommodityById(id);

        if (result == null) {
            throw new Error();
        }
        return result;
    }

    getCommodityFromCacheByName(name: string): Commodity {
        if (!name) {
            throw new Error("Invalid data.");
        }

        var result = this.getCommodityByName(name);

        if (result == null) {
            throw new Error();
        }
        return result;
    }
}
