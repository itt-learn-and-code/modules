﻿class SandwhichOptions {
    meat: Meat;
    bread: Bread;
    lettuce: boolean;
    tomato: boolean;
    pickles: boolean;
    bacon: boolean;
}

class SandwhichExampleSolution extends Example {
    sandwhichMaker(options: SandwhichOptions): void {
        var sandwhich = new Sandwhich(options.meat, options.bread, options.lettuce, options.tomato, options.pickles, options.bacon);
        // this should probably be moved into the sandwhich constructor
        this.setNutrition(sandwhich);
        this.tryToasting(sandwhich);
        // would rework this in the sandwhich class to process all different breads
        sandwhich.serve();
    }

    setNutrition(sandwhich: Sandwhich): void {
        sandwhich.isGlutenFree = sandwhich.bread == Bread.None;
        sandwhich.calories = this.getCalories(sandwhich);
    }

    getCalories(sandwhich: Sandwhich) {
        var totalCalories = this.calcCalories(sandwhich.meat);
        totalCalories += this.calcCalories(sandwhich.bread);
        totalCalories += sandwhich.lettuce ? this.calcCalories(Toppings.Lettuce) : 0;
        totalCalories += sandwhich.tomato ? this.calcCalories(Toppings.Tomato) : 0;
        totalCalories += sandwhich.pickles ? this.calcCalories(Toppings.Pickles) : 0;
        totalCalories += sandwhich.bacon ? this.calcCalories(Toppings.Bacon) : 0;
        return totalCalories;
    }

    tryToasting(sandwhich: Sandwhich): void {
        if (sandwhich.bread != Bread.None) {
            sandwhich.toToaster();
            while (!sandwhich.isToasted) {
                sandwhich.toast();
                // this could be reworked to change property in method instead of returning
                sandwhich.isToasted = this.isBreadToasted(sandwhich);
                if (this.isBreadBurnt(sandwhich))
                    break;
            }
        }
    }
}

class DetailsExampleSolution extends Example
{
    _repo = new Repository();
    _locationDataAdapterFactory = new LocationDataAdapterFactory();

    async GetDetailsById(placeId: string, sessionToken: string): Promise<LocationDataModel> {
        if (!this.requestIsValid(placeId, sessionToken))
            return null;

        try {
            var location = this._repo.getDetailsById(placeId, sessionToken);
            location = await this.validateLocation(location);
            return location?.asDataModel();
        }
        catch (error)
        {
            console.error(error);
        }

        return null;
    }

    requestIsValid(placeId: string, sessionToken: string): boolean {
        if (!placeId)
            return false;

        if (!sessionToken)
            return false;

        return true;
    }

    async validateLocation(location: LocationInformation): Promise<LocationInformation> {
        if (location != null) {
            location = this.addStatecodeMappingToNewLoction(location);

            var dbLocation = await this.createDatabaseLocation(location);
            var checkLocation = await this.createCheckLocation(location, dbLocation);
            this.setDatabaseLocationTimezoneFromLocation(dbLocation, location);
            if (dbLocation == null) {
                location = await this.addTimezoneRegionToNewLocationWithAdapterAsync(location);
            }
            location = await this.finalizeLocationWithCheck(location, checkLocation);
            return location;
        }
        return null;
    }

    async createDatabaseLocation(location: LocationInformation): Promise<LocationDataModel>
    {
        var adapter = this._locationDataAdapterFactory.create();
        var model = this.createModelFromLocation(location);
        return await adapter.getLocationAsync(model);
    }

    createModelFromLocation(location: LocationInformation, includeAddress: boolean = false): LocationDataModel {
        var locationModel = new LocationDataModel();
        if (includeAddress) {
            locationModel.address = location.address;
            locationModel.address2 = location.address2;
        }
        locationModel.city = location.city;
        locationModel.stateCode = location.stateCode;
        locationModel.zipCode = location.zipCode;
        locationModel.countryCode = location.countryCode;
        return locationModel;
    }

    async createCheckLocation(location: LocationInformation, dbLocation: LocationDataModel): Promise<LocationDataModel> {
        if (location.address) {
            var adapter = this._locationDataAdapterFactory.create();
            var model = this.createModelFromLocation(location, true);
            return await adapter.getLocationByAddressAsync(model);
        }
        else {
            return dbLocation;
        }
    }

    setDatabaseLocationTimezoneFromLocation(dbLocation: LocationDataModel, location: LocationInformation): void {
        if (dbLocation != null || dbLocation?.city) {
            location.timeZone = dbLocation.timeZone;
            location.region = dbLocation.region;
            location.area = dbLocation.area;
            location.areaNumber = dbLocation.areaNumber;
        }
    }

    async addTimezoneRegionToNewLocationWithAdapterAsync(location: LocationInformation): Promise<LocationInformation>
    {
        var adapter = this._locationDataAdapterFactory.create();
        return await this.addTimezoneRegionToNewLocationAsync(location, adapter);
    }

    async finalizeLocationWithCheck(location: LocationInformation, checkLocation: LocationDataModel): Promise<LocationInformation>
    {
        if (checkLocation == null || !checkLocation?.city) {
            var adapter = this._locationDataAdapterFactory.create();
            await adapter.insertAsync(location);
            return location;
        }
        else {
            return checkLocation;
        }
    }
}

class SampleRepositorySolution {
    _amazonClient: AmazonClient;
    CommodityData: CommodityData;

    constructor(amazonClient: AmazonClient) {
        this._amazonClient = amazonClient;
        this.CommodityData = new CommodityData(new CommodityCache(this));
    }

    async readData<T>(fileName: string): Promise<T[]>
    {
        try {
            if (!fileName) {
                throw new Error(fileName);
            }

            var json = await this._amazonClient.readDataFromBucket(fileName);

            var items = JSON.parse(json);
            if (!items)
                throw new Error("Invalid data");

            return items;
        }
        catch (error) {
            throw new Error(error);
        }
    }
}

class AmazonClient {
    _bucketName: string;

    constructor(bucketName: string) {
        this._bucketName = bucketName;
    }

    async readDataFromBucket(fileName: string): Promise<string> {
        try {
            if (this._bucketName == null) {
                throw new Error(`Invalid BucketName ${this._bucketName}`);
            }

            var request = new GetObjectRequest();
            request.bucketName = this._bucketName;
            request.key = fileName;

            var response = await AmazonS3Client.getObjectAsync(request);
            var memoryStream = new MemoryStream();
            response.responseStream.copyTo(memoryStream);
            memoryStream.position = 0;
            return getDataFromStream(memoryStream, response.headers.contentType);
        }
        catch (error) {
            throw new Error(error);
        }
    }
}

class CommodityCache {
    _repo: SampleRepositorySolution;
    commodityById: Map<number, Commodity>;
    commodityByName: Map<string, Commodity>;
    commodities: Commodity[];

    constructor(repo: SampleRepositorySolution) {
        this._repo = repo;
    }

    async reloadCommodityCache(fileName: string): Promise<void> {
        this.commodities = await this._repo.readData<Commodity>(fileName);
        this.commodityById.clear();
        this.commodityByName.clear();
        this.commodities.forEach(c => {
            this.commodityById[c.id] = c;
            this.commodityByName[c.name] = c;
        });
    }

    cacheCommodity(item: Commodity): void {
        this.commodityById[item.id] = item;
        this.commodityByName[item.name] = item;
    }

    getCommodityById(id: number): Commodity {
        return this.commodityById[id];
    }

    getCommodityByName(name: string): Commodity {
        return this.commodityByName[name];
    }
}

class CommodityData {
    _cache: CommodityCache;

    constructor(cache: CommodityCache) {
        this._cache = cache;
    }

    getCommoditiesFromCache = () => this._cache.commodities;

    getCommodityFromCache(id: number): Commodity {
        if (id == 0) {
            throw new Error("Invalid data.");
        }

        var result = this._cache.getCommodityById(id);

        if (result == null) {
            throw new Error();
        }
        return result;
    }

    getCommodityFromCacheByName(name: string): Commodity {
        if (!name) {
            throw new Error("Invalid data.");
        }

        var result = this._cache.getCommodityByName(name);

        if (result == null) {
            throw new Error();
        }
        return result;
    }
}
