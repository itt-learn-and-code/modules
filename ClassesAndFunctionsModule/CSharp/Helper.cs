﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassesAndFunctionsModule.CSharp
{
    public class Example
    {
        public int CalcCalories(Enum e)
        {
            return 100;
        }

        public bool IsBreadToasted(Sandwhich sandwhich)
        {
            return true;
        }

        public bool IsBreadBurnt(Sandwhich sandwhich)
        {
            return false;
        }

        protected Location AddStatecodeMappingToNewLoction(Location location)
        {
            return location;
        }

        protected async Task<Location> AddTimezoneRegionToNewLocationAsync(Location location, LocationAdapter adapter)
        {
            return await Task.FromResult(location);
        }
    }

    public class Sandwhich
    {
        public readonly Meat Meat;
        public readonly Bread Bread;
        public readonly bool Lettuce;
        public readonly bool Tomato;
        public readonly bool Pickles;
        public readonly bool Bacon;
        public bool IsGlutenFree { get; set; }
        public int Calories { get; set; }
        public bool IsToasted { get; set; }

        public Sandwhich(Meat meat, Bread bread, bool lettuce, bool tomato, bool pickles, bool bacon)
        {
            Meat = meat;
            Bread = bread;
            Lettuce = lettuce;
            Tomato = tomato;
            Pickles = pickles;
            Bacon = bacon;
        }

        public void ToToaster() { }
        public void Toast() { }
        public void ServeWithNoBread() { }
        public void ServeWithWhiteBread() { }
        public void ServeWithWheatBread() { }
        public void Serve() { }
    }

    public enum Meat
    {
        None, Ham, Turkey
    }

    public enum Bread
    {
        None, White, Wheat
    }

    public enum Toppings
    {
        Lettuce, Tomato, Pickles, Bacon
    }

    public class Location
    {
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZipCode { get; set; }
        public string CountryCode { get; set; }
        public string TimeZone { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string AreaNumber { get; set; }

        public LocationDataModel AsDataModel()
        {
            return new LocationDataModel();
        }
    }

    public class LocationDataModel : Location
    {
    }

    public class LocationAdapter
    {
        LocationDataModel checkLocation = null;

        public async Task<LocationDataModel> GetLocationAsync(LocationDataModel location)
        {
            return await Task.FromResult(location);
        }

        public async Task<LocationDataModel> GetLocationByAddressAsync(LocationDataModel location)
        {
            return await Task.FromResult(location);
        }

        public Task InsertAsync(Location location)
        {
            return Task.CompletedTask;
        }
    }

    public class Repository
    {
        public Location GetDetailsById(string placeId, string sessionToken, CancellationToken cancellationToken)
        {
            return new Location();
        }
    }
    public class LocationDataAdapterFactory
    {
        public LocationAdapter Create()
        {
            return new LocationAdapter();
        }
    }
    public class DataStream
    {
        public Stream Stream { get; set; }
    }

    public class GetObjectRequest
    {
        public string BucketName { get; set; }
        public string Key { get; set; }
    }

    public class AmazonResponse : IDisposable
    {
        public ResponseStream ResponseStream { get; set; }
        public Headers Headers { get; set; }

        public void Dispose()
        {
        }
    }

    public class ResponseStream
    {
        public void CopyTo(MemoryStream stream)
        {

        }
    }

    public class Headers
    {
        public string ContentType { get; set; }
    }

    public class MemoryStreamWithContentType : DataStream
    {
        public MemoryStreamWithContentType(MemoryStream stream, string content)
        {

        }
    }

    public static class AmazonS3Client
    {
        public static async Task<AmazonResponse> GetObjectAsync(GetObjectRequest request)
        {
            return await Task.FromResult(new AmazonResponse());
        }
    }

    public class Commodity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Equipment
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Definition { get; set; }
        public string Category { get; set; }
    }

    public class EquipmentOption
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class FreightClass
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public string Weight { get; set; }
        public string Description { get; set; }
    }

    public class TransportationMode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string PartialOrFull { get; set; }
        public int Rank { get; set; }
    }
    public class InvalidDataException : Exception { }
}
