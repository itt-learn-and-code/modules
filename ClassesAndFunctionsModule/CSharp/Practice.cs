﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassesAndFunctionsModule.CSharp
{
    public class Practice
    {
        #region Function
        public class SandwhichExample : Example
        {
            public void SandwhichMaker(Meat meat, Bread bread, bool lettuce, bool tomato, bool pickles, bool bacon)
            {
                Sandwhich sandwhich = new Sandwhich(meat, bread, lettuce, tomato, pickles, bacon);
                sandwhich.IsGlutenFree = bread == Bread.None;
                sandwhich.Calories = CalcCalories(meat) + CalcCalories(bread) + (lettuce ? CalcCalories(Toppings.Lettuce) : 0) + (tomato ? CalcCalories(Toppings.Tomato) : 0) + (pickles ? CalcCalories(Toppings.Pickles) : 0) + (bacon ? CalcCalories(Toppings.Bacon) : 0);
                if (sandwhich.Bread != Bread.None)
                {
                    sandwhich.ToToaster();
                    while (!sandwhich.IsToasted)
                    {
                        sandwhich.Toast();
                        sandwhich.IsToasted = IsBreadToasted(sandwhich);
                        if (IsBreadBurnt(sandwhich))
                            break;
                    }
                }
                switch (sandwhich.Bread)
                {
                    case Bread.None:
                        sandwhich.ServeWithNoBread();
                        break;
                    case Bread.White:
                        sandwhich.ServeWithWhiteBread();
                        break;
                    case Bread.Wheat:
                        sandwhich.ServeWithWheatBread();
                        break;
                    default:
                        sandwhich.Serve();
                        break;
                }
            }
        }

        public class DetailsExample : Example
        {
            Repository _repo = new Repository();
            LocationDataAdapterFactory _locationDataAdapterFactory = new LocationDataAdapterFactory();

            public async Task<LocationDataModel> GetDetailsById(string placeId, string sessionToken, CancellationToken cancellationToken = default)
            {
                if (string.IsNullOrEmpty(placeId))
                    return null;

                if (string.IsNullOrEmpty(sessionToken))
                    return null;

                if (cancellationToken.IsCancellationRequested)
                    return null;

                if (!Guid.TryParse(sessionToken, out Guid x))
                    return null;

                try
                {
                    Location location = _repo.GetDetailsById(placeId, sessionToken, cancellationToken);

                    if (location != null)
                    {
                        location = AddStatecodeMappingToNewLoction(location);

                        LocationDataModel dbLocation = null;
                        LocationDataModel checkLocation = null;

                        LocationAdapter adapter = _locationDataAdapterFactory.Create();
                        dbLocation = await adapter.GetLocationAsync(new LocationDataModel
                        {
                            City = location.City,
                            StateCode = location.StateCode,
                            ZipCode = location.ZipCode,
                            CountryCode = location.CountryCode
                        });

                        if (!string.IsNullOrEmpty(location.Address))
                        {
                            adapter = _locationDataAdapterFactory.Create();
                            checkLocation = await adapter.GetLocationByAddressAsync(new LocationDataModel
                            {
                                Address = location.Address,
                                Address2 = location.Address2,
                                City = location.City,
                                StateCode = location.StateCode,
                                ZipCode = location.ZipCode,
                                CountryCode = location.CountryCode
                            });
                        }

                        if (string.IsNullOrEmpty(location.Address))
                        {
                            checkLocation = dbLocation;
                        }

                        if (dbLocation != null || !string.IsNullOrEmpty(dbLocation?.City))
                        {
                            location.TimeZone = dbLocation.TimeZone;
                            location.Region = dbLocation.Region;
                            location.Area = dbLocation.Area;
                            location.AreaNumber = dbLocation.AreaNumber;
                        }

                        if (dbLocation == null)
                        {
                            adapter = _locationDataAdapterFactory.Create();
                            location = await AddTimezoneRegionToNewLocationAsync(location, adapter);
                        }

                        if (checkLocation == null || string.IsNullOrEmpty(checkLocation?.City))
                        {
                            adapter = _locationDataAdapterFactory.Create();
                            await adapter.InsertAsync(location);
                        }
                        else
                        {
                            location = checkLocation;
                        }

                        return location.AsDataModel();
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine($"Failed to get place details by place id: {placeId}", ex);
                }

                return null;
            }

        }
        #endregion

        #region Class
        public class SampleRepository
        {
            private readonly string _bucketName;

            public SampleRepository(string bucketName)
            {
                _bucketName = bucketName;
            }

            public async Task<List<T>> ReadData<T>()
            {
                try
                {
                    var fileName = $"{typeof(T).Name}.json";
                    if (string.IsNullOrEmpty(fileName))
                    {
                        throw new Exception(fileName);
                    }

                    var data = await ReadAsStreamAsync(fileName);

                    using (var reader = new StreamReader(data.Stream))
                    {
                        var json = reader.ReadToEnd();

                        var items = JsonConvert.DeserializeObject<List<T>>(json) ?? throw new InvalidDataException();

                        return items;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  while reading document {@fileName}", ex);
                }
            }

            public async Task<DataStream> ReadAsStreamAsync(string fileName)
            {
                try
                {
                    if (_bucketName == null)
                    {
                        throw new Exception($"Invalid BucketName {_bucketName}");
                    }

                    var request = new GetObjectRequest
                    {
                        BucketName = _bucketName,
                        Key = fileName
                    };

                    using (var response = await AmazonS3Client.GetObjectAsync(request))
                    {
                        var memoryStream = new MemoryStream();
                        response.ResponseStream.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        return new MemoryStreamWithContentType(memoryStream, response.Headers.ContentType);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(ReadAsStreamAsync)} failed", ex);
                }
            }

            private ConcurrentDictionary<int, Commodity> CommodityById { get; set; }
            private ConcurrentDictionary<string, Commodity> CommodityByName { get; set; }
            public List<Commodity> CachedCommodities { get; set; }

            public async Task ReloadCommodityCache()
            {
                CachedCommodities = await ReadData<Commodity>();
                Parallel.ForEach(CachedCommodities, CacheCommodity);
            }

            private void CacheCommodity(Commodity item)
            {
                CommodityById.TryAdd(item.Id, item);
                CommodityByName.TryAdd(item.Name, item);
            }

            public Commodity GetCommodity(int id)
            {
                CommodityById.TryGetValue(id, out Commodity result);
                return result;
            }

            public Commodity GetCommodity(string name)
            {
                CommodityByName.TryGetValue(name, out Commodity result);
                return result;
            }

            public List<Commodity> GetCommoditiesFromCache() => CachedCommodities;

            public Commodity GetCommodityFromCache(int id)
            {
                if (id == 0 || id == default(int))
                {
                    throw new InvalidDataException();
                }

                var result = GetCommodity(id);

                if (result == null)
                {
                    throw new Exception();
                }
                return result;
            }

            public Commodity GetCommodityFromCache(string name)
            {
                if (string.IsNullOrEmpty(name))
                {
                    throw new InvalidDataException();
                }

                var result = GetCommodity(name);

                if (result == null)
                {
                    throw new Exception();
                }
                return result;
            }
        }
        #endregion
    }
}
