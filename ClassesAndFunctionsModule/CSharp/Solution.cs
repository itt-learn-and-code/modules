﻿using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ClassesAndFunctionsModule.CSharp
{
    public class Solution
    {
        #region Function
        public class SandwhichOptions
        {
            public Meat Meat { get; set; }
            public Bread Bread { get; set; }
            public bool Lettuce { get; set; }
            public bool Tomato { get; set; }
            public bool Pickles { get; set; }
            public bool Bacon { get; set; }
        }

        public class SandwhichExample : Example
        {
            public void SandwhichMaker(SandwhichOptions options)
            {
                Sandwhich sandwhich = new Sandwhich(options.Meat, options.Bread, options.Lettuce, options.Tomato, options.Pickles, options.Bacon);
                // this should probably be moved into the sandwhich constructor
                SetNutrition(sandwhich);
                TryToasting(sandwhich);
                // would rework this in the sandwhich class to process all different breads
                sandwhich.Serve();
            }

            private void SetNutrition(Sandwhich sandwhich)
            {
                sandwhich.IsGlutenFree = sandwhich.Bread == Bread.None;
                sandwhich.Calories = GetCalories(sandwhich);
            }

            private int GetCalories(Sandwhich sandwhich)
            {
                int totalCalories = CalcCalories(sandwhich.Meat);
                totalCalories += CalcCalories(sandwhich.Bread);
                totalCalories += sandwhich.Lettuce ? CalcCalories(Toppings.Lettuce) : 0;
                totalCalories += sandwhich.Tomato ? CalcCalories(Toppings.Tomato) : 0;
                totalCalories += sandwhich.Pickles ? CalcCalories(Toppings.Pickles) : 0;
                totalCalories += sandwhich.Bacon ? CalcCalories(Toppings.Bacon) : 0;
                return totalCalories;
            }

            private void TryToasting(Sandwhich sandwhich)
            {
                if (sandwhich.Bread != Bread.None)
                {
                    sandwhich.ToToaster();
                    while (!sandwhich.IsToasted)
                    {
                        sandwhich.Toast();
                        // this could be reworked to change property in method instead of returning
                        sandwhich.IsToasted = IsBreadToasted(sandwhich);
                        if (IsBreadBurnt(sandwhich))
                            break;
                    }
                }
            }
        }

        public class DetailsExample : Example
        {
            Repository _repo = new Repository();
            LocationDataAdapterFactory _locationDataAdapterFactory = new LocationDataAdapterFactory();

            public async Task<LocationDataModel> GetDetailsById(string placeId, string sessionToken, CancellationToken cancellationToken = default)
            {
                if (!RequestIsValid(placeId, sessionToken, cancellationToken))
                    return null;

                try
                {
                    Location location = _repo.GetDetailsById(placeId, sessionToken, cancellationToken);
                    location = await ValidateLocation(location);
                    return location?.AsDataModel();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine($"Failed to get place details by place id: {placeId}", ex);
                }

                return null;
            }

            public bool RequestIsValid(string placeId, string sessionToken, CancellationToken cancellationToken = default)
            {
                if (string.IsNullOrEmpty(placeId))
                    return false;

                if (string.IsNullOrEmpty(sessionToken))
                    return false;

                if (cancellationToken.IsCancellationRequested)
                    return false;

                if (!Guid.TryParse(sessionToken, out Guid x))
                    return false;

                return true;
            }

            public async Task<Location> ValidateLocation(Location location)
            {
                if (location != null)
                {
                    location = AddStatecodeMappingToNewLoction(location);

                    LocationDataModel dbLocation = await CreateDatabaseLocation(location);
                    LocationDataModel checkLocation = await CreateCheckLocation(location, dbLocation);
                    SetDatabaseLocationTimezoneFromLocation(dbLocation, location);
                    if (dbLocation == null)
                    {
                        location = await AddTimezoneRegionToNewLocationAsync(location);
                    }
                    location = await FinalizeLocationWithCheck(location, checkLocation);
                    return location;
                }
                return null;
            }

            async Task<LocationDataModel> CreateDatabaseLocation(Location location)
            {
                LocationAdapter adapter = _locationDataAdapterFactory.Create();
                return await adapter.GetLocationAsync(new LocationDataModel
                {
                    City = location.City,
                    StateCode = location.StateCode,
                    ZipCode = location.ZipCode,
                    CountryCode = location.CountryCode
                });
            }

            async Task<LocationDataModel> CreateCheckLocation(Location location, LocationDataModel dbLocation)
            {
                if (!string.IsNullOrEmpty(location.Address))
                {
                    var adapter = _locationDataAdapterFactory.Create();
                    return await adapter.GetLocationByAddressAsync(new LocationDataModel
                    {
                        Address = location.Address,
                        Address2 = location.Address2,
                        City = location.City,
                        StateCode = location.StateCode,
                        ZipCode = location.ZipCode,
                        CountryCode = location.CountryCode
                    });
                }
                else
                {
                    return dbLocation;
                }
            }

            void SetDatabaseLocationTimezoneFromLocation(LocationDataModel dbLocation, Location location)
            {
                if (dbLocation != null || !string.IsNullOrEmpty(dbLocation?.City))
                {
                    location.TimeZone = dbLocation.TimeZone;
                    location.Region = dbLocation.Region;
                    location.Area = dbLocation.Area;
                    location.AreaNumber = dbLocation.AreaNumber;
                }
            }

            async Task<Location> AddTimezoneRegionToNewLocationAsync(Location location)
            {
                var adapter = _locationDataAdapterFactory.Create();
                return await AddTimezoneRegionToNewLocationAsync(location, adapter);
            }

            async Task<Location> FinalizeLocationWithCheck(Location location, LocationDataModel checkLocation)
            {
                if (checkLocation == null || string.IsNullOrEmpty(checkLocation?.City))
                {
                    var adapter = _locationDataAdapterFactory.Create();
                    await adapter.InsertAsync(location);
                    return location;
                }
                else
                {
                    return checkLocation;
                }
            }
        }
        #endregion

        #region Class
        public class SampleRepository
        {
            private readonly AmazonClient _amazonClient;
            public CommodityData CommodityData;

            public SampleRepository(AmazonClient amazonClient)
            {
                _amazonClient = amazonClient;
                CommodityData = new CommodityData(new CommodityCache(this));
            }

            public async Task<List<T>> ReadData<T>()
            {
                try
                {
                    var fileName = $"{typeof(T).Name}.json";
                    if (string.IsNullOrEmpty(fileName))
                    {
                        throw new Exception(fileName);
                    }

                    var data = await _amazonClient.ReadAsStreamAsync(fileName);

                    using (var reader = new StreamReader(data.Stream))
                    {
                        var json = reader.ReadToEnd();

                        var items = JsonConvert.DeserializeObject<List<T>>(json) ?? throw new InvalidDataException();

                        return items;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  while reading document {@fileName}", ex);
                }
            }
        }

        public class AmazonClient
        {
            private readonly string _bucketName;

            public AmazonClient(string bucketName)
            {
                _bucketName = bucketName;
            }

            public async Task<DataStream> ReadAsStreamAsync(string fileName)
            {
                try
                {
                    if (_bucketName == null)
                    {
                        throw new Exception($"Invalid BucketName {_bucketName}");
                    }

                    var request = new GetObjectRequest
                    {
                        BucketName = _bucketName,
                        Key = fileName
                    };

                    using (var response = await AmazonS3Client.GetObjectAsync(request))
                    {
                        var memoryStream = new MemoryStream();
                        response.ResponseStream.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        return new MemoryStreamWithContentType(memoryStream, response.Headers.ContentType);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{nameof(ReadAsStreamAsync)} failed", ex);
                }
            }
        }

        public class CommodityCache
        {
            private readonly SampleRepository _repo;
            private ConcurrentDictionary<int, Commodity> CommodityById { get; set; }
            private ConcurrentDictionary<string, Commodity> CommodityByName { get; set; }
            public List<Commodity> Commodities { get; set; }

            public CommodityCache(SampleRepository repo)
            {
                _repo = repo;
            }

            public async Task ReloadCommodityCache()
            {
                Commodities = await _repo.ReadData<Commodity>();
                Parallel.ForEach(Commodities, CacheCommodity);
            }

            private void CacheCommodity(Commodity item)
            {
                CommodityById.TryAdd(item.Id, item);
                CommodityByName.TryAdd(item.Name, item);
            }

            public Commodity GetCommodity(int id)
            {
                CommodityById.TryGetValue(id, out Commodity result);
                return result;
            }

            public Commodity GetCommodity(string name)
            {
                CommodityByName.TryGetValue(name, out Commodity result);
                return result;
            }
        }

        public class CommodityData
        {
            private readonly CommodityCache _cache;

            public CommodityData(CommodityCache cache)
            {
                _cache = cache;
            }

            public List<Commodity> GetCommoditiesFromCache() => _cache.Commodities;

            public Commodity GetCommodityFromCache(int id)
            {
                if (id == 0 || id == default(int))
                {
                    throw new InvalidDataException();
                }

                var result = _cache.GetCommodity(id);

                if (result == null)
                {
                    throw new Exception();
                }
                return result;
            }

            public Commodity GetCommodityFromCache(string name)
            {
                if (string.IsNullOrEmpty(name))
                {
                    throw new InvalidDataException();
                }

                var result = _cache.GetCommodity(name);

                if (result == null)
                {
                    throw new Exception();
                }
                return result;
            }
        }
        #endregion
    }
}
