﻿using RefinementModule;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LearnAndCodeTests
{
    public class RefinementModuleTests
    {
        Practice practiceModule = new Practice();

        [Fact]
        public void CreateStudentSuccessTest()
        {
            string studentName = "Test Student";
            string studentGrade = "75";
            practiceModule.CreateStudent(studentName, studentGrade);
            Assert.Single(practiceModule.students);
            Assert.Equal(studentName, practiceModule.students[0].Item1);
            Assert.Equal(decimal.Parse(studentGrade), practiceModule.students[0].Item2);
        }

        [Fact]
        public void CreateStudentFailureTest()
        {
            string studentName = "Test Student";
            string studentGrade = "bad";
            practiceModule.CreateStudent(studentName, studentGrade);
            Assert.Empty(practiceModule.students);
        }

        [Fact]
        public void ListStudentsTest()
        {
            string[] studentNames =
            {
                "First Student",
                "Second Student",
                "Third Student",
                "Fourth Student",
                "Fifth Student"
            };
            string[] studentGrades =
            {
                "50", "60", "70", "80", "90"
            };
            for (int i = 0; i < studentNames.Length; i++)
            {
                practiceModule.CreateStudent(studentNames[i], studentGrades[i]);
            }
            Assert.Equal(studentNames.Length, practiceModule.students.Count);
            for (int i = 0; i < studentNames.Length; i++)
            {
                Assert.Equal(studentNames[i], practiceModule.students[i].Item1);
                Assert.Equal(decimal.Parse(studentGrades[i]), practiceModule.students[i].Item2);
            }
            using (var outputFileWriter = new StreamWriter("listoutput.txt"))
            {
                Console.SetOut(outputFileWriter);
                practiceModule.ListAllStudents();
            }
            using (var outputFileReader = new StreamReader("listoutput.txt"))
            {
                var expectedOutput = "Current students:\r\n";
                foreach (var s in practiceModule.students)
                {
                    expectedOutput += $"{s.Item1} - {s.Item2}\r\n";
                }
                Assert.Equal(expectedOutput, outputFileReader.ReadToEnd());
            }
        }

        [Fact]
        public void EditStudentSuccessTest()
        {
            string studentName = "Test Student";
            string studentGrade = "75";
            string editedStudentName = "Edited Student";
            string editedStudentGrade = "100";
            practiceModule.CreateStudent(studentName, studentGrade);
            Assert.Single(practiceModule.students);
            Assert.Equal(studentName, practiceModule.students[0].Item1);
            Assert.Equal(decimal.Parse(studentGrade), practiceModule.students[0].Item2);
            practiceModule.EditStudent("1", editedStudentName, editedStudentGrade);
            Assert.Single(practiceModule.students);
            Assert.Equal(editedStudentName, practiceModule.students[0].Item1);
            Assert.Equal(decimal.Parse(editedStudentGrade), practiceModule.students[0].Item2);
        }

        [Fact]
        public void EditStudentFailureTest()
        {
            string studentName = "Test Student";
            string studentGrade = "75";
            string editedStudentName = "Edited Student";
            string editedStudentGrade = "bad";
            practiceModule.CreateStudent(studentName, studentGrade);
            Assert.Single(practiceModule.students);
            Assert.Equal(studentName, practiceModule.students[0].Item1);
            Assert.Equal(decimal.Parse(studentGrade), practiceModule.students[0].Item2);
            practiceModule.EditStudent("1", editedStudentName, editedStudentGrade);
            Assert.Single(practiceModule.students);
            Assert.Equal(studentName, practiceModule.students[0].Item1);
            Assert.Equal(decimal.Parse(studentGrade), practiceModule.students[0].Item2);
        }

        [Fact]
        public void DeleteStudentTest()
        {
            string studentName = "Test Student";
            string studentGrade = "75";
            practiceModule.CreateStudent(studentName, studentGrade);
            Assert.Single(practiceModule.students);
            practiceModule.DeleteStudent("1");
            Assert.Empty(practiceModule.students);
        }
    }
}
