﻿const reader = require('readline-sync');

module.exports.students = [];
module.exports.main = function () {
    console.log("Choose operation:");
    console.log("1 - Create Student");
    console.log("2 - List All Students");
    console.log("3 - Edit Student");
    console.log("4 - Delete Student");
    console.log("5 - Quit");
    var choice = reader.question("Enter choice: ");
    switch (choice) {
        case '1':
            this.createStudent();
            break;
        case '2':
            this.listAllStudents();
            break;
        case '3':
            this.editStudent();
            break;
        case '4':
            this.deleteStudent();
            break;
        case '5':
            process.exit();
            break;
        default:
            console.log("Invalid answer!");
    }
    this.main();
};



module.exports.createStudent = function (name = null, grade = null) {
    if (name == null) {
        name = reader.question("Enter student's name: ");
    }

    if (grade == null) {
        grade = reader.question("Enter student's grade percentage: ");
    }
    var parsedGrade = parseFloat(grade);
    if (isNaN(parsedGrade)) {
        console.log("Could not parse grade.");
    }
    else {
        this.students.push([name, parsedGrade]);
    }
};

module.exports.listAllStudents = function() {
    console.log("Current students:");
    this.students.forEach(s =>
        console.log(`${s[0]} - ${s[1]}`));
};

module.exports.editStudent = function(choice = null, name = null, grade = null) {
    if (choice == null) {
        console.log("Choose a student to edit:");
        count = 1;
        this.students.forEach(s => {
            console.log(`${count} - ${s[0]} - ${s[1]}`);
            count++;
        });

        choice = reader.question("Enter choice: ");
    }
    var parsedChoice = parseInt(choice);
    if (parsedChoice) {
        var student = this.students[parsedChoice - 1];
        if (name == null) {
            name = reader.question(`Enter student's name (Current - ${student[0]}): `);
        }
        if (grade == null) {
            grade = reader.question(`Enter student's grade percentage (Current - ${student[1]}): `);
        }
        var parsedGrade = parseFloat(grade);
        if (parsedGrade) {
            this.students[parsedChoice - 1] = [name, parsedGrade];
        }
        else {
            console.log("Could not parse grade");
        }
    }
};

module.exports.deleteStudent = function(choice = null) {
    if (choice == null) {
        console.log("Choose a student to edit:");
        count = 1;
        this.students.forEach(s => {
            console.log(`${count} - ${s[0]} - ${s[1]}`);
            count++;
        });

        choice = reader.question("Enter choice: ");
    }
    var parsedChoice = parseInt(choice);
    if (parsedChoice) {
        this.students.splice(parsedChoice - 1, 1);
    }
};

