﻿const assert = require('chai').assert;
const practice = require('../practice');

describe('Program', function() {
    describe('CreateStudent', function() {
        it('should add student', function() {
            practice.students = [];

            var studentName = "Test Student";
            var studentGrade = "75";

            practice.createStudent(studentName, studentGrade);
            assert.equal(practice.students.length, 1);
            assert.equal(studentName, practice.students[0][0]);
            assert.equal(parseFloat(studentGrade), practice.students[0][1]);
        });

        it('shouldn\'t add student with invalid information', function() {
            practice.students = [];
            var studentName = "Test Student";
            var studentGrade = "bad";

            practice.createStudent(studentName, studentGrade);
            assert.equal(practice.students.length, 0);
        })
    });

    describe('ListAllStudents', function() {
        it('should list students', function() {
            practice.students = [];
            var studentNames = [
                "First Student",
                "Second Student",
                "Third Student",
                "Fourth Student",
                "Fifth Student"
            ];
            var studentGrades = ["50", "60", "70", "80", "90"];
            for (i = 0; i < studentNames.length; i++) {
                practice.createStudent(studentNames[i], studentGrades[i]);
            }
            assert.equal(studentNames.length, practice.students.length);
            for (i = 0; i < studentNames.length; i++) {
                assert.equal(studentNames[i], practice.students[i][0]);
                assert.equal(parseFloat(studentGrades[i]), practice.students[i][1]);
            }
            output = "";
            console.log = function(message) {
                output += message;
            };
            practice.listAllStudents();
            var expectedOutput = "Current students:";
            practice.students.forEach(s => 
                expectedOutput += `${s[0]} - ${s[1]}`);
            assert.equal(expectedOutput, output);
        });
    });

    describe('EditStudent', function () {
        it('should edit student', function () {
            practice.students = [];
            studentName = "Test Student";
            studentGrade = "75";
            editedStudentName = "Edited Student";
            editedStudentGrade = "100";
            practice.createStudent(studentName, studentGrade);
            assert.equal(1, practice.students.length);
            assert.equal(studentName, practice.students[0][0]);
            assert.equal(parseFloat(studentGrade), practice.students[0][1]);
            practice.editStudent("1", editedStudentName, editedStudentGrade);
            assert.equal(1, practice.students.length);
            assert.equal(editedStudentName, practice.students[0][0]);
            assert.equal(parseFloat(editedStudentGrade), practice.students[0][1]);
        });

        it('shouldn\'t edit student with invalid information', function () {
            practice.students = [];
            studentName = "Test Student";
            studentGrade = "75";
            editedStudentName = "Edited Student";
            editedStudentGrade = "bad";
            practice.createStudent(studentName, studentGrade);
            assert.equal(1, practice.students.length);
            assert.equal(studentName, practice.students[0][0]);
            assert.equal(parseFloat(studentGrade), practice.students[0][1]);
            practice.editStudent("1", editedStudentName, editedStudentGrade);
            assert.equal(1, practice.students.length);
            assert.equal(studentName, practice.students[0][0]);
            assert.equal(parseFloat(studentGrade), practice.students[0][1]);
        });
    });

    describe('DeleteStudent', function () {
        it('should delete student', function () {
            practice.students = [];
            studentName = "Test Student";
            studentGrade = "75";
            practice.createStudent(studentName, studentGrade);
            assert.equal(1, practice.students.length);
            practice.deleteStudent("1");
            assert.equal(0, practice.students.length);
        });
    });
});