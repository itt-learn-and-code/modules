﻿using System;
using System.Collections.Generic;

namespace RefinementModule
{
    public class Practice
    {
        public List<Tuple<string, decimal>> students = new List<Tuple<string, decimal>>();

        public void Main()
        {
            bool quit = false;
            while (!quit)
            {
                Console.WriteLine("Choose operation:");
                Console.WriteLine("1 - Create Student");
                Console.WriteLine("2 - List All Students");
                Console.WriteLine("3 - Edit Student");
                Console.WriteLine("4 - Delete Student");
                Console.WriteLine("5 - Quit");
                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        CreateStudent();
                        break;
                    case "2":
                        ListAllStudents();
                        break;
                    case "3":
                        EditStudent();
                        break;
                    case "4":
                        DeleteStudent();
                        break;
                    case "5":
                        quit = true;
                        break;
                    default:
                        Console.WriteLine("Invalid selection.");
                        break;
                }
            }
        }

        public void CreateStudent(string name = null, string grade = null)
        {
            Console.WriteLine("Enter student's name:");
            if (name == null)
                name = Console.ReadLine();
            Console.WriteLine("Enter student's grade percentage:");
            if (grade == null)
                grade = Console.ReadLine();
            if (decimal.TryParse(grade, out decimal g))
            {
                students.Add(new Tuple<string, decimal>(name, g));
            }
            else
            {
                Console.WriteLine("Could not parse grade");
            }
        }

        public void ListAllStudents()
        {
            Console.WriteLine("Current students:");
            foreach (var s in students)
            {
                Console.WriteLine($"{s.Item1} - {s.Item2}");
            }
        }

        public void EditStudent(string choice = null, string name = null, string grade = null)
        {
            Console.WriteLine("Choose a student to edit:");
            for (int i = 1; i < students.Count; i++)
            {
                Console.WriteLine($"{i} - {students[i].Item1} - {students[i].Item2}");
            }
            if (choice == null)
                choice = Console.ReadLine();
            if (int.TryParse(choice, out int c))
            {
                var student = students[c - 1];
                Console.WriteLine($"Enter student's name (Current - {student.Item1}):");
                if (name == null)
                    name = Console.ReadLine();
                Console.WriteLine($"Enter student's grade percentage (Current - {student.Item2}):");
                if (grade == null)
                    grade = Console.ReadLine();
                if (decimal.TryParse(grade, out decimal g))
                {
                    students[c - 1] = new Tuple<string, decimal>(name, g);
                }
                else
                {
                    Console.WriteLine("Could not parse grade");
                }
            }
        }

        public void DeleteStudent(string choice = null)
        {
            Console.WriteLine("Choose a student to delete:");
            for (int i = 1; i < students.Count; i++)
            {
                Console.WriteLine($"{i} - {students[i].Item1} - {students[i].Item2}");
            }
            if (choice == null)
                choice = Console.ReadLine();
            if (int.TryParse(choice, out int c))
            {
                students.RemoveAt(c - 1);
            }
        }
    }
}
